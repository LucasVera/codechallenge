# Matter supply code challenge
Author: Lucas Vera Toro (lucas@lucasdev.info)
This project is an 8-hour implementation of the code challenge provided by Matter Supply. Challenge can be found [HERE](https://github.com/mattersupply/code-challenge)

## How to run the application
1. Download the code using `git clone`
2. Navigate to the directory where you cloned the repository
3. type in a terminal `npm install`
4. Create a file in the root directory called `.env.development` and add the following contents to it

`REACT_APP_GITHUB_API_URL = https://api.github.com`

6. type `npm start`
7. Open the following url in a browser http://localhost:3000
  


##  Tech stack
Aside from the mandatory technology (React, designs and fonts), the following technologies/frameworks/libraries were used to speed up the development process
- **CSS framework**: Bootstrap
- **Router**: react-router-dom
- **Global state library**: Redux
- **Fetch library**: Axios
- **Date library**: moment
- **Testing**: Jest + Enzyme
- **React boilerplate**: Create React App
- **Documentation**: Jira, Confluence
- **Code repository**: Bitbucket

### Pending implementation
- **Hosting**: [Netlify](https://www.netlify.com)
- **Automated CI/CD**: Bitbucket's Pipelines, CircleCI
- **Logging and Monitoring**: [Datadog](https://www.datadoghq.com)




## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br  />

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br  />

You will also see any lint errors in the console.

  

### `npm test`

Launches the test runner in the interactive watch mode.<br  />

See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

  

### `npm run build`

Builds the app for production to the `build` folder.<br  />

It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br  />

Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

  

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

  

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

  

