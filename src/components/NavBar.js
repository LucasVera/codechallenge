import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'

export default class NavBar extends Component {
  static propTypes = {
    prop: PropTypes
  }

  render() {
    // Get user from redux if it's already logged. Depending on that, the user's avatar would be shown
    // instead of the "Sign-in" button at the top-right corner
    return (
      <nav className="navbar navbar-expand-sm navbar-light d-flex align-items-center justify-content-between">
        <Link className="navbar-brand" to="/">LOGO</Link>
        <div className="border-bottom border-primary pb-2 border-5" style={{ cursor: 'pointer' }} onClick={() => alert('"Sign in" still not implemented :(')}>
          Sign in
        </div>
      </nav>
    )
  }
}
