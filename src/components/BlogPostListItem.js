import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { getMonthsAgo } from '../services/util';
import { Link } from 'react-router-dom';
import moment from 'moment';
import BlogPostDate from './BlogPostDate';

export default class BlogPostListItem extends Component {
  static propTypes = {
    post: PropTypes.object,
    className: PropTypes.string
  }

  render() {
    
    const {
      className,
      post: {
        id,
        description,
        updatedAt
      }
    } = this.props;
    return (
      <li className={`${className} list-group-item d-flex flex-column border-0`}>
        <span className="small"><small>
          <BlogPostDate postDate={updatedAt} />
        </small></span>
        <div className="d-flex justify-content-between">
          <span id="desc">{description}</span>
          <Link to={`/details/${id}`}>Read</Link>
        </div>
      </li>
    )
  }
}
