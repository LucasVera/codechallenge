import React, { Component } from 'react'
import PropTypes from 'prop-types'

export default class PostOwner extends Component {
  static propTypes = {
    owner: PropTypes.object,
    className: PropTypes.string
  }

  render() {
    const { className, owner: { avatarUrl, name } } = this.props;
    return (
      <div className={`${className} d-flex align-items-start justify-content-start flex-row`}>
        <img className="img-fluid" src={avatarUrl} style={{ height: '60px' }} alt={`${name} avatar`} />
        <div className="d-flex align-items-start justify-content-end flex-column ml-3">
          <h5>{name}</h5>
          <p className="text-primary">Posts</p>
        </div>
      </div>
    )
  }
}
