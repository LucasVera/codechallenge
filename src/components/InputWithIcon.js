import React, { Component } from 'react'
import PropTypes from 'prop-types'

export default class InputWithIcon extends Component {
  static propTypes = {
    className: PropTypes.string,
    iconClass: PropTypes.string,
    placeholder: PropTypes.string,
    onChange: PropTypes.func,
    onClick: PropTypes.func,
    value: PropTypes.string
  }

  handleOnKeyUp(e) {
    const { onClick, value } = this.props;
    if (e.keyCode === 13) {
      if (value) {
        onClick();
      }
    }
  }

  render() {
    const { className, iconClass, placeholder, onChange, value } = this.props;
    return (
      <div className={`
          ${className}
          input-group
          bg-secondary
          my-2 pl-3
          border border-secondary round
          d-flex justify-content-center align-items-center
        `}>
        <span className={`${iconClass}`}></span>
        <input
          type="text"
          className="form-control bg-secondary border-0 ml-1"
          placeholder={placeholder}
          onChange={onChange}
          onKeyUp={(e) => this.handleOnKeyUp(e)}
          value={value}
        />
      </div>
    )
  }
}
