import React, { Component } from 'react'
import PropTypes from 'prop-types'

export default class Loading extends Component {
  static propTypes = {
    className: PropTypes.string,
    isFetching: PropTypes.bool
  }

  render() {
    // (TODO): Spinner?
    const { isFetching, className } = this.props;
    if (!isFetching) return <div></div>;
    return (
      <div className={className}>
        <h3>Loading...</h3>
      </div>
    );
  }
}
