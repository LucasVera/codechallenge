import React from 'react';
import BlogPostDate from '../BlogPostDate';
import { mount } from 'enzyme';
import { getMonthsAgo } from '../../services/util';

it('Shows correct date format and months ago', () => {
  const date = '2020-07-01';
  const monthsAgo = `${getMonthsAgo(date)} months ago`;
  const formattedDate = 'July 01 / 2020';

  const element = mount(<BlogPostDate postDate={date} />);
  const dateElement = element.find('i#date');
  const monthsElement = element.find('i#months');

  expect(dateElement.text()).toBe(formattedDate);
  expect(monthsElement.text()).toBe(monthsAgo);
  
  element.unmount()
});
