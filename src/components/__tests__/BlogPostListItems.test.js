import React from 'react';
import { mount } from 'enzyme';
import BlogPostListItem from '../BlogPostListItem';
import { BrowserRouter } from 'react-router-dom';

it('Shows correct description', () => {
  const description = 'test_desc';
  const element = mount(
    <BrowserRouter>
      <BlogPostListItem post={{ description }} />
    </BrowserRouter>
  );

  const descriptionSpan = element.find('span#desc');
  expect(descriptionSpan.text()).toBe(description);

  element.unmount()
});
