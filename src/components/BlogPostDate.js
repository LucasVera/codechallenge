import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { getMonthsAgo } from '../services/util';
import moment from 'moment';

export default class BlogPostDate extends Component {
  static propTypes = {
    className: PropTypes.string,
    postDate: PropTypes.string
  }

  render() {
    const { className, postDate } = this.props;
    const monthsAgo = getMonthsAgo(postDate);
    const formattedDate = moment(postDate).format('MMMM DD / YYYY');
    return (
      <div>
        <i id="date">{formattedDate}</i>
        <i id="dot" className="mx-1 text-primary">•</i>
        <i id="months" className="text-primary">{monthsAgo} months ago</i>
      </div>
    )
  }
}
