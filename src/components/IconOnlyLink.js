import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom';

export default class IconOnlyButton extends Component {
  static propTypes = {
    to: PropTypes.string,
    className: PropTypes.string,
    iconClass: PropTypes.string,
    show: PropTypes.bool,
    onClick: PropTypes.func
  }

  render() {
    const { className, to, iconClass, show, onClick } = this.props;
    if (!show) return <div></div>;
    return (
      <Link id="link_element" className={className} to={to} onClick={onClick}>
        <span id="icon_span" className={iconClass} />
      </Link>
    )
  }
}
