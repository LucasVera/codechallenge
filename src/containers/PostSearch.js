import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { searchUserGists } from '../services/reduxModules/blogPosts'
import VRSvg from '../components/SVGs/VRSvg';
import InputWithIcon from '../components/InputWithIcon';
import BlogPostListItem from '../components/BlogPostListItem';
import Loading from '../components/Loading';
import PostOwner from '../components/PostOwner';
import PropTypes from 'prop-types';

export class PostSearch extends Component {
  static propTypes = {
    initialUsername: PropTypes.string
  }

  state = {
    username: ''
  }

  componentDidMount() {
    if (this.props.initialUsername) {
      this.setState({ username: this.props.initialUsername });
    }

  }

  handleSearchPosts(keyword) {
    const { searchUserGists } = this.props;
    searchUserGists(keyword);
  }

  handleUsernameInputChange(e) {
    let { value } = e.target;
    value = value.replace(/[^\w\s]/gi, '');
    this.setState({ username: value });
  }

  render() {
    const { isFetching, error, posts, owner } = this.props;
    const { username } = this.state;
    return (
      <div className="container-fluid">
        <div className="row mt-5">
          <div className="col-12 col-md-5 offset-md-1">
            <div className="d-flex flex-column justify-content-center">
              <h1 className="display-3 text-primary">Blog msco.</h1>
              <p className="mt-5 mb-3">Explore the unknown. Uncover what matters. Prototype, test, repeat. 
                Combine intuition with evidence. Design with intent ant build it right.
              </p>
              <div>
                <InputWithIcon
                  id="username_input"
                  className=""
                  onChange={(e) => this.handleUsernameInputChange(e)}
                  value={username}
                  iconClass="fas fa-search text-primary"
                  placeholder="Keyword..."
                  onClick={() => this.handleSearchPosts(username)}
                  validateEmpty={true}
                />
                {error &&
                  <div class="alert alert-danger square py-4" role="alert">
                    <strong>{error}</strong>
                  </div>
                }
                {owner && owner.name
                  ? <PostOwner owner={owner} className="my-5"/>
                  : <button
                      className="btn btn-primary btn-lg px-5 mt-4"
                      onClick={() => this.handleSearchPosts(username)}
                      disabled={!username}
                    >
                      Search
                    </button>
                }
                <Loading className="p-4" isFetching={isFetching} />
                {posts && Array.isArray(posts) && posts.map(post => (
                  <ul className="list-group my-3">
                    <BlogPostListItem
                      post={post}
                      className=""
                    />
                  </ul>
                ))}
              </div>
            </div>
          </div>

          <div className="col-12 col-md-6">
            <VRSvg className="d-flex" />
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({
  blogPosts: {
    isFetching,
    error,
    posts,
    owner
  }
}) => ({
  isFetching,
  error,
  posts,
  owner
});

const mapDispatchToProps = dispatch => bindActionCreators({
  searchUserGists
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(PostSearch)
