import React from 'react';
import { mount } from 'enzyme';
import PostSearch from '../PostSearch';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';

const middlewares = []
const mockStore = configureStore(middlewares)
const initialState = { blogPosts: {} };

it('Updates username when username_input is changed', () => {
  const initialUsername = 'test_usernam';

  const element = mount(
    <Provider store={mockStore(initialState)}>
      <PostSearch initialUsername={initialUsername} />
    </Provider>
  );
  
  const postSearchElement = element.find('PostSearch');

  let inputWithIconElement = postSearchElement.find('InputWithIcon#username_input');
  inputWithIconElement.find('input').simulate('change',
    { target: { value: `${initialUsername}e` } }
  );

  expect(postSearchElement.state().username).toBe('test_username');

  element.unmount();
});

it('Does not update username when special character is inputted', () => {
  const initialUsername = 'test_usernam';

  const element = mount(
    <Provider store={mockStore(initialState)}>
      <PostSearch initialUsername={initialUsername} />
    </Provider>
  );
  
  const postSearchElement = element.find('PostSearch');

  let inputWithIconElement = postSearchElement.find('InputWithIcon#username_input');
  inputWithIconElement.find('input').simulate('change', {
    target: { value: `${initialUsername}*/` } }
  );

  expect(postSearchElement.state().username).toBe(initialUsername);

  element.unmount();
});

