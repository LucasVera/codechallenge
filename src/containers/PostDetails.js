import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux';
import { searchUserGists, getGistByRawUrl } from '../services/reduxModules/blogPosts';
import ShortBioSvg from '../components/SVGs/ShortBioSvg';
import BlogPostDate from '../components/BlogPostDate';
import PostOwner from '../components/PostOwner';
import IconOnlyLink from '../components/IconOnlyLink';
import Loading from '../components/Loading';
import { Link } from 'react-router-dom';

export class PostDetails extends Component {
  state = {
    validationError: ''
  }
  renderErrorMsg(msg) {
    return (
      <div className="d-flex justify-content-center align-items-center flex-column" style={{ height: '100vh' }}>
        <h3>Can't show this post right now: {msg}</h3>
        <h3><Link to="/">Go to search page</Link></h3>
      </div>
    );
  }

  componentDidMount() {
    const { post, id } = this.getPostFromCache();
    if (!id) return this.setState({ validationError: `Empty id: ${id}` });
    if (!post) return this.setState({ validationError: `Post with id ${id} not found` });

    this.getGist(post);
  }

  getGist(post) {
    const {
      file: { rawUrl },
      rawText,
    } = post;

    if (!rawText && rawUrl && post.id) {
      this.props.getGistByRawUrl(post.id, rawUrl);
    }
  }

  getPostFromCache() {
    const {
      posts,
      match: { params: { id } }
    } = this.props;
    if (!id) return { id };
    const post = posts.find(p => p.id === id);
    if (!post) return { id, post };

    return { post, id };
  }

  getNextAndPreviousPost(id) {
    const { posts } = this.props;
    let previous, next
    const postIndex = posts.findIndex(post => post.id === id);
    if (postIndex > 0) {
      previous = posts[postIndex - 1];
    }
    if (postIndex < posts.length - 1) {
      next = posts[postIndex + 1];
    }

    return { previous, next };
  }

  render() {
    const { isFetching, error, posts } = this.props;
    const { validationError } = this.state;

    const { id, post } = this.getPostFromCache();

    if (validationError || error) {
      return this.renderErrorMsg(validationError || error);
    }

    if (!posts || !Array.isArray(posts)) {
      return this.renderErrorMsg('No posts loaded. Please, search for posts before trying to display one');
    }

    if (!id || !post) {
      return this.renderErrorMsg('Id or post not found.');
    }

    const {
      description,
      file: { filename },
      updatedAt,
      rawText,
      owner
    } = post;

    const { previous, next } = this.getNextAndPreviousPost(id);

    return (
      <div className="container-fluid">
        <div className="row mt-5">
          <div className="col-12 col-md-6 offset-md-1">
            <div className="d-flex flex-column justify-content-center">
              <BlogPostDate postDate={updatedAt} />
              <h1 className="display-5 text-primary">{description}</h1>
              <code className="small mt-5"><small>{filename}</small></code>
              <code className="code border">
                {rawText || <Loading isFetching={isFetching} className="m-5" />}
              </code>
              <PostOwner className="mt-5" owner={owner} />
            </div>
          </div>

          <div className="col-12 col-md-5">
            <ShortBioSvg className="d-flex align-items-start" style={{ height: '400px', width: 'auto' }} />
          </div>
        </div>
        <div className="row">
          <div className="col-12">
            <div className="d-flex justify-content-center align-items-center my-2">
              <IconOnlyLink
                to={`/details/${previous && previous.id}`}
                show={!!previous}
                className="mx-4"
                iconClass="fas fa-arrow-left fa-2x"
                onClick={() => this.getGist(previous)}
              />
              <IconOnlyLink
                to={`/details/${next && next.id}`}
                show={!!next}
                className="mx-4"
                iconClass="fas fa-arrow-right fa-2x"
                onClick={() => this.getGist(next)}
              />
            </div>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = ({
  blogPosts: {
    posts,
    isFetching,
    error
  }
}) => ({
  posts,
  isFetching,
  error
});

const mapDispatchToProps = dispatch => bindActionCreators({
  searchUserGists, getGistByRawUrl
}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(PostDetails)
