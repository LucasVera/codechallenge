import moment from 'moment';

export const getMonthsAgo = (date) => {
  return moment().diff(moment(date), 'months');
}

export const sleepMs = (ms) => {
  return new Promise((resolve) => {
    setTimeout(() => {
      return resolve()
    }, ms);
  })
}
