export const transformFiles = (rawFiles) => {
  /*
  // correct way to handle all files.
  const files = [];
  Object.keys(rawFiles).forEach((key) => {
    const { filename, type, raw_url } = rawFiles[key];
    files.push({
      name: filename,
      type: type,
      rawUrl: raw_url,
    });
  });
  */

  const { filename, type, raw_url } = rawFiles[Object.keys(rawFiles)[0]];

  return { file: { filename, type, rawUrl: raw_url } };
}
