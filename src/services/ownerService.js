export const transformOwnerData = (ownerData) => {
  if (!ownerData) {
    return {};
  }

  const {
    avatar_url,
    name,
  } = ownerData;

  const owner = {
    avatarUrl: avatar_url,
    name,
  };

  return { owner };
}

export const addOwnerToPosts = (owner, posts) => {
  return posts.map(post => ({
    ...post,
    owner
  }));
}
