import { combineReducers } from 'redux'
import blogPosts from './blogPosts';

export default combineReducers({
  blogPosts
});
