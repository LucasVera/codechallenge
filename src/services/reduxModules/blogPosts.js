import axios from 'axios';
import { getOwnerUrlFromPostData, transformBlogPostsData, addRawTextToPost } from '../blogPostsService';
import { transformOwnerData, addOwnerToPosts } from '../ownerService';

const GITHUB_API_URL = process.env.REACT_APP_GITHUB_API_URL;

// CONSTANTS
export const SEARCH_USER_GISTS_REQUEST = 'blogs/SEARCH_USER_GISTS/request';
export const SEARCH_USER_GISTS_SUCCESS = 'blogs/SEARCH_USER_GISTS/success';
export const SEARCH_USER_GISTS_FAILURE = 'blogs/SEARCH_USER_GISTS/failure';

export const GET_GIST_BY_RAW_URL_REQUEST = 'blogs/GET_GIST_BY_RAW_URL/request';
export const GET_GIST_BY_RAW_URL_SUCCESS = 'blogs/GET_GIST_BY_RAW_URL/success';
export const GET_GIST_BY_RAW_URL_FAILURE = 'blogs/GET_GIST_BY_RAW_URL/failure';

const initialState = {
  isFetching: false,
  error: '',
  owner: {},
  posts: []
}
// REDUCER
export default (state = initialState, { type, payload }) => {
  switch (type) {
    case SEARCH_USER_GISTS_REQUEST:
      return {
        ...state,
        isFetching: true,
        error: ''
      }
    case SEARCH_USER_GISTS_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: payload.error
      }
    case SEARCH_USER_GISTS_SUCCESS:
      
      return {
        ...state,
        isFetching: false,
        posts: payload.blogPosts,
        owner: payload.owner
      }
  
    case GET_GIST_BY_RAW_URL_REQUEST:
      return {
        ...state,
        isFetching: true,
        error: ''
      }
    case GET_GIST_BY_RAW_URL_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: payload.error
      }
    case GET_GIST_BY_RAW_URL_SUCCESS:
      return {
        ...state,
        isFetching: false,
        posts: addRawTextToPost(state.posts, payload.postId, payload.data).newBlogPosts
      }

    default:
      return state;
  }
}


// ACTIONS

/**
 * Searches in github gists for public gists for the specified username
 * Saves the gists in the posts and the owner. each post also has a reference
 * to the owner
 * @param {String} username - Username in github to search gists
 */
export const searchUserGists = (username) => async (dispatch) => {
  try {
    dispatch({ type: SEARCH_USER_GISTS_REQUEST });

    if (!GITHUB_API_URL) throw 'Github API Url is not configured!';

    const { data: blogPostsData } = await axios.get(`${GITHUB_API_URL}/users/${username}/gists?page=1&per_page=5`);
    if (!blogPostsData) {
      throw "No response from server. Please, try again later.";
    }

    if (blogPostsData.length <= 0) {
      throw 'No results. try with a different username';
    }

    const { url } = getOwnerUrlFromPostData(blogPostsData);
    if (!url) {
      throw "Owner's url wasn't found";
    }
    
    const { data: ownerData } = await axios.get(url);

    const { blogPosts } = transformBlogPostsData(blogPostsData);
    const { owner } = transformOwnerData(ownerData);

    dispatch({ type: SEARCH_USER_GISTS_SUCCESS, payload: { blogPosts: addOwnerToPosts(owner, blogPosts), owner } });
  }
  catch (ex) {
    // a not found (404) error from the server gives out an error, which is not ideal
    // It's better to transform it to an empty array so the system can handle it
    // normally (showing that no results were found, instead of an ugly 404 error)
    // Also, would be better to handle not_found_username vs no_gists_found in the code
    console.log(`Error in blogs module when searching users gists. username: ${username}`, ex);
    dispatch({ type: SEARCH_USER_GISTS_FAILURE, payload: { error: ex.message } }); // Better error handling
  }
}


/**
 * Gets a gist by its raw url to display in the app.
 * @param {String} rawUrl - Raw url for the gist. Must be previously fetched
 */
export const getGistByRawUrl = (postId, rawUrl) => async (dispatch) => {
  try {
    dispatch({ type: GET_GIST_BY_RAW_URL_REQUEST });

    const { data } = await axios.get(rawUrl, { responseType: 'text' });
    if (!data) {
      throw 'No response from server';
    }

    dispatch({ type: GET_GIST_BY_RAW_URL_SUCCESS, payload: { data, postId } });

  }
  catch (ex) {
    console.log(`Error in blogs module when getting gist by raw url. rawUrl: ${rawUrl}`, ex);
    dispatch({ type: GET_GIST_BY_RAW_URL_FAILURE, payload: { error: ex.message } });
  }
}

