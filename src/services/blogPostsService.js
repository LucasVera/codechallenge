import { transformFiles } from "./fileService";

const blogPostsEmpty = (blogPostsData) => {
  return !blogPostsData
    || !Array.isArray(blogPostsData)
    || blogPostsData.length <= 0;
}

export const transformBlogPostsData = (blogPostsData) => {
  if (blogPostsEmpty(blogPostsData)) {
    return {};
  }

  const blogPosts = blogPostsData.map(({
    id,
    updated_at,
    description,
    files: rawFiles,
  }) => ({
      id,
      description: description || transformFiles(rawFiles).file.filename,
      file: transformFiles(rawFiles).file,
      updatedAt: updated_at,
    })
  );

  return { blogPosts };
}

export const getOwnerUrlFromPostData = (blogPostsData) => {
  if (blogPostsEmpty(blogPostsData)) {
    return {};
  }

  const {
    url
  } = blogPostsData[0].owner;

  return { url };
}


export const addRawTextToPost = (blogPosts, postId, rawText) => {
  // (TODO): this would be better done through fileId instead of postId
  // But for the sake of simplicity, would be left like that for now
  if (blogPostsEmpty(blogPosts)) {
    return {};
  }

  const newBlogPosts = blogPosts.map(cachePost =>
    cachePost.id === postId
      ? { ...cachePost, rawText }
      : cachePost
  );

  return { newBlogPosts };
}
