import React from 'react';
import logo from './logo.svg';
import './assets/css/bootstrap.css';
import './assets/css/custom.css';
import NavBar from './components/NavBar';

// Routes
import { Route } from 'react-router';
import PostSearch from './containers/PostSearch';
import PostDetails from './containers/PostDetails';



function App() {
  return (
    <div>
      <NavBar />
      <Route exact path="/" component={PostSearch} />
      <Route exact path="/details/:id" component={PostDetails} />
    </div>
  );
}

export default App;
